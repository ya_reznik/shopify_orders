import time
from functools import wraps
from urllib.error import HTTPError


def control_many_requests(func):
    @wraps(func)
    def wrapper(self, *args, **kwargs):
        result = None
        attempts = 5
        while attempts > 0:
            result = None
            try:
                result = func(self, *args, **kwargs)
            except HTTPError as e:
                if e.code == 429:
                    time.sleep(2)
            if result:
                break

            attempts -= 1

        return result

    return wrapper

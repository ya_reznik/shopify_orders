""" Shopify Orders Receiver

Class for getting orders.
We must get orders that were created OR updated during the time period and therefore we must create two separate queries
one query for getting created orders in period and the second for getting updated orders in period.

"""
from datetime import datetime, timedelta
from urllib import parse

import shopify

from consts import API_VERSION_2019_04, LIMIT_250
from decorators import control_many_requests


class OrdersReceiver:
    """Class for getting orders."""

    def __init__(self, shop_name, token, api_version=API_VERSION_2019_04, limit=LIMIT_250):
        """
        Init object
        :param shop_name: your shop name
        :param token: valid token
        :param api_version: api version, default value = last available version at the moment
        :param limit: count orders on page, by default it equals 250
        """
        self.shop_name = shop_name
        self.api_version = api_version
        self.limit = limit
        self.token = token

        self._activate_session()

    def _activate_session(self):
        """Start working with shopify api."""
        session = shopify.Session("{name}.myshopify.com".format(name=self.shop_name),
                                  version=self.api_version,
                                  token=self.token)
        shopify.ShopifyResource.activate_session(session)

    def _date_to_iso_format(self, date):
        """
        Convert date format to iso
        :param date: input date for converting
        :return: date in iso format
        """
        return date.isoformat(timespec='microseconds')

    def _get_dates(self, start_days_back, number_of_days):
        """
        Get start and end dates from input parameters.
        :param start_days_back: count days to back
        :param number_of_days: gap in days
        :return: date start and date end for queries
        """
        now = datetime.now().date()
        now = datetime(now.year, now.month, now.day, 23, 59, 50)
        date_end = now - timedelta(days=start_days_back)
        date_start = date_end - timedelta(days=number_of_days)
        date_start = datetime(date_start.year, date_start.month, date_start.day, 0, 0, 0)

        date_start = self._date_to_iso_format(date_start)
        date_end = self._date_to_iso_format(date_end)
        return date_start, date_end

    @control_many_requests
    def get_orders_on_page(self, start_days_back, number_of_days, param_name='created', to_json=False):
        """
        Get orders on page by python generator
        :param start_days_back: count days to back
        :param number_of_days: gap in days
        :param param_name: created or updated
        :param to_json: convert orders to json
        :return: order list
        """
        date_start, date_end = self._get_dates(start_days_back, number_of_days)
        page = 1
        order_filters = {
            'status': 'any',
            'limit': self.limit,
            'page': page,
            '{}_at_min'.format(param_name): date_start,
            '{}_at_max'.format(param_name): date_end,
        }
        count_orders = self.limit
        while count_orders == self.limit:
            orders = shopify.Order.find(**order_filters)
            count_orders = len(orders)
            if orders:
                if to_json:
                    orders = self.orders_to_json(orders)
                yield orders
                page += 1
                order_filters['page'] = page

    def get_created_orders_on_page(self, start_days_back, number_of_days, to_json=False):
        """
        Get created orders on page by python generator during period on one page
        :param start_days_back: count days to back
        :param number_of_days: gap in days
        :param to_json: convert orders to json
        :return: created orders
        """
        return self.get_orders_on_page(start_days_back, number_of_days, 'created', to_json)

    def get_updated_orders_on_page(self, start_days_back, number_of_days, to_json=False):
        """
        Get updated orders on page by python generator during period on one page
        :param start_days_back: count days to back
        :param number_of_days: gap in days
        :param to_json: convert orders to json
        :return: updated orders
        """
        return self.get_orders_on_page(start_days_back, number_of_days, 'updated', to_json)

    def orders_to_json(self, orders):
        """
        Get orders as json
        :param orders:
        :return: json with orders data
        """
        orders_json = [order.to_json() for order in orders]
        return orders_json

    def get_orders(self, start_days_back, number_of_days, to_json=True):
        """
        Get all orders during period on all pages
        :param start_days_back: count days to back
        :param number_of_days: gap in days
        :param to_json: convert orders to json
        :return: all orders
        """
        orders = [order for orders in self.get_orders_on_page(start_days_back, number_of_days) for order in orders]
        created_orders_id = [order.id for order in orders]
        updated_orders = [order for orders in self.get_updated_orders_on_page(start_days_back, number_of_days)
                          for order in orders if order.id not in created_orders_id]
        orders.extend(updated_orders)

        if to_json:
            orders = self.orders_to_json(orders)
        return orders

    def get_token(self, api_key, secret_key, redirect_url):
        """
        Get token for working with app
        :param api_key: api key from shop
        :param secret_key: secret key from shop
        :param redirect_url: redirect uri from shop
        :return: valid token
        """
        session = shopify.Session("{name}.myshopify.com".format(name=self.shop_name),
                                  version=self.api_version)
        session.setup(api_key=api_key, secret=secret_key)

        scope = ['read_orders', 'read_products', 'read_customers', ]
        permission_url = session.create_permission_url(scope, redirect_url)
        print('Please, go to url in your browser: ', permission_url)

        url = input('Input redirected link for grab:')
        params = dict(parse.parse_qsl(parse.urlsplit(url).query))
        return session.request_token(params)

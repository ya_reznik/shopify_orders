"""
Examples for working with tool.
"""
from receivers import OrdersReceiver

shop_name = input('Input shop name(required): ')
token = input('Input token(required): ')

api_key = input('Input Api key(not required): ')
secret_key = input('Input secret key(not required): ')
redirect_url = input('Input redirect url(not required): ')

if shop_name and token:
    receiver = OrdersReceiver(shop_name=shop_name, token=token, limit=250)

    # if you want to get token it will be helpful
    # print(receiver.get_token(api_key, secret_key, redirect_url))

    # get all orders from all pages
    receiver.get_orders(start_days_back=7, number_of_days=0)

    # if you want to work with orders by generators you must merge lists of created and updated orders manually
    # because we get them by two different queries and we can equal and merge only after getting all data

    # updated orders
    # get orders on a page
    for orders in receiver.get_updated_orders_on_page(start_days_back=7, number_of_days=0, to_json=True):
        print(orders)

    # created orders
    # get orders on a page
    for orders in receiver.get_created_orders_on_page(start_days_back=7, number_of_days=0, to_json=True):
        print(orders)
